(function () {
    'use strict';

    // Http Provider
    function runUtilities($location, $rootScope, tmhDynamicLocale, $translate, $state, IMAGE_SIZE, ngMeta, $timeout) {

        // Gets a varibale if it is a prerender Agent
        $rootScope.isPrerenderAgent = (navigator.userAgent.toLowerCase().indexOf('prerender') != -1) ? true : false;

        // Preview Hash Functionality
        $rootScope.previewHash = ($location.$$search.preview_hash != undefined) ? '&preview_hash=' +$location.$$search.preview_hash : '';

        // Checks the preview Hash when changing routes if it was originally added
        if ($rootScope.previewHash != '') {
            $rootScope.$on("$stateChangeSuccess",
                function (event, toState, toParams, fromState, fromParams) {
                    $rootScope.previewHash = ($location.$$search.preview_hash != undefined) ? '&preview_hash=' +$location.$$search.preview_hash : '';
                }
            );
        }


        // Sets the locale language
        $timeout(function () {
            tmhDynamicLocale.set($translate.use());
        }, 0);


        $rootScope.$onMany = function(events, fn) {
            for(var i = 0; i < events.length; i++) {
                this.$on(events[i], fn);
            }
        };

        // create UTILS functions to be used in the project //TODO check where to use them
        $rootScope.UTIL = {

            getActiveClass: function (page) {

                var current = $state.params.identifier;

                return 'top.' + page === current ? "active" : "";
            },

            getNestedChildren: function (arr, parent) {
                var out = [];

                for (var i in arr) {
                    if (arr[i].parentId == parent) {
                        var children = this.getNestedChildren(arr, arr[i].id);

                        if (children.length) {
                            arr[i].children = children;
                            arr[i].c = true;

                        }
                        out.push(arr[i]);
                    }
                }
                return out;
            },

            IMAGE_SIZE: IMAGE_SIZE,

            addSize: function (name, obj) {

                var returnedObj = $rootScope.UTIL.IMAGE_SIZE;
                returnedObj[name] = obj;

                return returnedObj;
            },

            slugify: function (text) {

                return text.toString().toLowerCase()
                    .replace(/\s+/g, '-')
                    .replace(/[^\w\-]+/g, '')
                    .replace(/\-\-+/g, '-')
                    .replace(/^-+/, '')
                    .replace(/-+$/, '');
            }

        };

        ngMeta.init();

    }

    runUtilities.$inject =['$location', '$rootScope', 'tmhDynamicLocale', '$translate', '$state', 'IMAGE_SIZE', 'ngMeta', '$timeout'];

    angular
        .module('bravoureAngularApp')
        .run(runUtilities);

})();
