# Bravoure - Run Utilities Http

## Use

It is used to load :

Preview Hash Variable - loads the hash detected in the url for previewing the page
Prerender Agent Variable - loads true if the Prerender Agent is detected


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-run-utilities": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
