## Angular Run Utilies / Bravoure component

### **Versions:**

1.0.4 - Added Slugify method
1.0.3 - Fixing bug of previous version
1.0.2 - Added component run-utilities from jello
1.0.1 - Update url parameter to preview_hash
1.0.0 - Initial Stable version

---------------------
